# -*- coding: utf-8 -*-
"""
Created on Wed Jul 31 09:13:01 2019

@author: andre
"""

# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 17:51:46 2019

@author: andre
"""

# %% librerias
import numpy as np
from MIfunctions import wait_BCI, leer_openbci
from sklearn.externals import joblib  # https://joblib.readthedocs.io/en/latest/
# %% importar modelo .pkl
path = 'data_openbci_gcpds/'
name = '2019-07-19 17_41_51.820255.pkl'
filename = path + name
my_model_loaded = joblib.load(filename)  # DIFF
best_model = my_model_loaded[0]

# %% leer data desde csv -> Cargar desde openci()
name = '2019-07-19 17_41_51.820255.csv'
filename_train = path + name
Ch = np.arange(0, 15)  # np.arange(7,15)
vt = [-0.5, 3]  # 2*sfreq otf = 2 #5*sfreq
clases = [30, 40]
fs = 190
Xraw, y, sfreq = leer_openbci(filename_train, clases, Ch, vt)
print(Xraw.shape)

nepochs = np.size(Xraw, axis=0)
Ch = np.size(Xraw, axis=1)
T = np.size(Xraw, axis=2)
# %%
import random
nums = [x for x in range(nepochs)]
random.shuffle(nums)
Xraw = Xraw[nums, :, :]
y = y[nums]

# %% pyinput--> pip install pynput
from pynput.keyboard import Controller  # Key
import time
#fac_max = 0.8
left = 'a'
right = 'd'
#tmin = 0.2
tmax = 1
keyboard = Controller()
ii = 0

while ii < nepochs:  # while true or input = xxxx
    print('epoch %d/%d\n' % (ii + 1, nepochs))
    # leer datos OPENBCI Xraw = readopenbci_gcpds() # 1 x ch  x T

    #y_pro = best_model.predict_proba(Xraw[ii,:,:].reshape(1,Ch,T))
    y_pro = best_model.predict(Xraw[ii, :, :].reshape(1, Ch, T))

    if (y_pro == 1):
        keyboard.press(left)
        print('%s__epoch:%d' % (left, ii + 1))
        time.sleep(tmax)
        keyboard.release(left)

    elif (y_pro == 2):
        keyboard.press(right)
        print('%s__epoch:%d' % (right, ii + 1))
        time.sleep(tmax)
        keyboard.release(right)

    ii += 1
# %%

y_all = best_model.predict(Xraw)
acc = 100 * np.sum(y == y_all) / len(y)
print('acierto total=%.2f' % acc)
