# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 16:22:15 2019

@author: andre
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 19:36:41 2019
@author: andre
"""
# %% librerias
import numpy as np
from MIfunctions import leer_openbci, bank_filter_epochsEEG, CSP_epochs_filter_extractor
import matplotlib.pyplot as plt
import scipy.io
import time

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier

from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split, GridSearchCV, cross_val_score
from sklearn.metrics import confusion_matrix, f1_score, accuracy_score
from sklearn.externals import joblib  # https://joblib.readthedocs.io/en/latest/
# %% leer data desde csv
path = 'data_openbci_gcpds/'
name = '2019-07-19 17_41_51.820255.csv'
filename_train = path + name
Ch = np.arange(0, 16)  # np.arange(7,15)
vt = [-0.5, 3]  # 2*sfreq otf = 2 #5*sfreq
clases = [30, 40]
fs = 190
Xraw, y, sfreq = leer_openbci(filename_train, clases, Ch, vt)
print(Xraw.shape)
# %% banco filtros + csp + pca visualizacion
#f_frec = np.array([[4,8],[8,12],[12,16],[16,20],[20,24],[24,28]])
f_frec = np.array([[8, 12], [12, 30]])
ncomp = 8
import warnings
warnings.filterwarnings("ignore")
csp_epochs = CSP_epochs_filter_extractor(fs=fs, f_frec=f_frec, ncomp=ncomp)
csp_epochs.fit(Xraw, y)

scaler = StandardScaler()
Xcsp = csp_epochs.transform(Xraw)
Xdataz = scaler.fit_transform(Xcsp)
pca = PCA(n_components=min(Xcsp.shape))
tsne = TSNE(n_components=2, perplexity=10.0)
#Z = tsne.fit_transform(Xdataz)
Z = pca.fit_transform(Xdataz)
plt.scatter(Z[:, 0], Z[:, 1], c=y)
plt.show()
Xdataz.shape
# %% entrenar modelo basado en csp y svc

start_time = time.time()
steps = [('CSP_EEG', CSP_epochs_filter_extractor(fs=fs, f_frec=f_frec)),
         ('scaler', StandardScaler()), ('PCA', PCA(n_components=0.975)), ('SVM', SVC(probability=True))]

# steps = [('CSP_EEG',  CSP_epochs_filter_extractor(fs=fs,f_frec=f_frec)),
#         ('scaler', StandardScaler()), ('SVM', SVC(probability=True))]

pipeline = Pipeline(steps)  # define the pipeline object.
X_train, X_test, y_train, y_test = train_test_split(Xraw, y, test_size=0.1, random_state=30, stratify=y)

# busqueda parametros por CV grilla
parameters = {'CSP_EEG__ncomp': [4, 8, 10], 'SVM__C': [0.1, 10, 100, 1e3], 'SVM__gamma': [0.1, 0.01, 1]}
grid_search = GridSearchCV(pipeline, parameters, n_jobs=4, cv=10, scoring='accuracy', verbose=20)
grid_search.fit(X_train, y_train)
print("--- %.2f seconds ---" % (time.time() - start_time))
# update joblib conda Anaconda 3 -> conda install joblib, conda update joblib
# % mejor modelo
best_model = grid_search.best_estimator_
print(grid_search.best_params_)

# evaluar mejor modelo segun test desde split

y_test_e = best_model.predict(X_test)
cm = confusion_matrix(y_test, y_test_e)
cm = 100 * cm / np.sum(cm, 1)
print('\nConfusion matrix' + filename_train)
print(cm)
print("F1=%.4f" % f1_score(y_test, y_test_e))
print("Acc=%.4f" % accuracy_score(y_test, y_test_e))

# %% modelo validacion cruzada
best_model = grid_search.best_estimator_
scores = cross_val_score(best_model, Xraw, y, cv=10, n_jobs=4, scoring='accuracy')  # 'f1')https://scikit-learn.org/stable/modules/model_evaluation.html
scores
# %% Guardar modelo
best_all = [best_model, filename_train[:-4]]
joblib.dump(best_all, filename_train[:-4] + ".pkl")  # DIFF  https://docs.python.org/2/library/pickle.html
# ...
# my_model_loaded = joblib.load(filename_train+".pkl") # DIFF
#y = my_model_loaded(Xraw)
