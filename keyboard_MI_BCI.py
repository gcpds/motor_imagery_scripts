# -*- coding: utf-8 -*-
"""
Created on Fri Jul 26 17:51:46 2019

@author: andre
"""

# %% librerias
import numpy as np
from MIfunctions import wait_BCI, leer_openbci
from sklearn.externals import joblib  # https://joblib.readthedocs.io/en/latest/
# %% importar modelo .pkl
path = 'data_openbci_gcpds/'
name = '2019-07-19 17_41_51.820255.pkl'
filename = path + name
my_model_loaded = joblib.load(filename)  # DIFF
best_model = my_model_loaded[0]

# %% leer data desde csv -> Cargar desde openci()
name = '2019-07-19 17_41_51.820255.csv'
filename_train = path + name
Ch = np.arange(0, 15)  # np.arange(7,15)
vt = [-0.5, 3]  # 2*sfreq otf = 2 #5*sfreq
clases = [30, 40]
fs = 190
Xraw, y, sfreq = leer_openbci(filename_train, clases, Ch, vt)
print(Xraw.shape)

nepochs = np.size(Xraw, axis=0)
Ch = np.size(Xraw, axis=1)
T = np.size(Xraw, axis=2)
# %%
import random
nums = [x for x in range(nepochs)]
random.shuffle(nums)
Xraw = Xraw[nums, :, :]

# %% pyinput--> pip install pynput
from pynput.keyboard import Controller  # Key
fac_max = 0.8
left = 'c'
right = 'd'
tmin = 0.2
tmax = 1
keyboard = Controller()
ii = 0

while ii < nepochs:  # while true or input = xxxx
    print('epoch %d/%d\n' % (ii + 1, nepochs))
    # leer datos OPENBCI Xraw = readopenbci_gcpds() # 1 x ch  x T

    y_pro = best_model.predict_proba(Xraw[ii, :, :].reshape(1, Ch, T))
    imax = np.argmax(y_pro)
    if (y_pro[0][imax] > fac_max) and (imax == 0):
        keyboard.press(left)
        print('%s__epoch:%d--prob=%.2f' % (left, ii + 1, y_pro[0][imax]))
        ns = wait_BCI(y_pro[0][imax], fac_max, tmax, tmin)
        print('  ns=%.2f' % ns)
        keyboard.release(left)

    elif (y_pro[0][imax] > fac_max) and (imax == 1):
        keyboard.press(right)
        print('%s__epoch:%d--prob=%.2f' % (right, ii + 1, y_pro[0][imax]))
        ns = wait_BCI(y_pro[0][imax], fac_max, tmax, tmin)
        print('  ns=%.2f\n' % ns)
        keyboard.release(right)
    ii += 1
# %%
