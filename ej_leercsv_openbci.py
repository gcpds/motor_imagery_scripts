# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 20:59:17 2019

@author: andre
"""
import numpy as np
from MIfunctions import leer_openbci

#%%
path = 'data_openbci_gcpds/'
name = '2019-07-19 17_41_51.820255.csv'
filename_train = path+name
Ch = np.arange(0,15)#np.arange(7,15)
vt = [-0.5,3]#2*sfreq otf = 2 #5*sfreq
clases = [30,40]
fs = 190
Xraw, y,sfreq = leer_openbci(filename_train,clases,Ch,vt)
#Xraw: arreglo con datos eeg -> intentos X canales X muestras
#y: vector etiquetas -> intentos x 1
#sfreq: frecuencia de muestreo
#%%