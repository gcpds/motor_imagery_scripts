# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 17:39:21 2019

@author:Usage
Here you can get help of any object by pressing Ctrl+I in front of it, either on the Editor or the Console.
 andre
"""

from scipy.signal import butter, lfilter, lfilter_zi  # , freqz
import numpy as np
from mne.io import read_raw_edf
from mne.decoding import CSP
import pandas as pd
import json as js  # conda install -c jmcmurray json


# %%
def leer_bci42a_train_full(path_filename, clases, Ch, vt):

    raw = read_raw_edf(path_filename, preload=False)
    sfreq = raw.info['sfreq']

    # raw.save('tempraw.fif',overwrite=True)#, tmin=3, tmax=5,overwrite = True)
    # rawo = mne.io.read_raw_fif('tempraw.fif', preload=True)  # load data
    # depurar canales
    # rawo.plot()

    # clases_b = [769,770,771,772] #codigo clases
    i_muestras_ = raw._raw_extras[0]['events'][1]           # Indices de las actividades.
    i_clases_ = raw._raw_extras[0]['events'][2]           # Marcadores de las actividades.

    remov = np.ndarray.tolist(i_clases_)                 # Quitar artefactos.
    Trials_eli = 1023                                   # Elimina los trials con artefactos.
    m = np.array([i for i, x in enumerate(remov) if x == Trials_eli])   # Identifica en donde se encuentra los artefactos.
    m_ = m + 1
    tt = np.array(raw._raw_extras[0]['events'][0] * [1], dtype=bool)
    tt[m] = False
    tt[m_] = False
    i_muestras = i_muestras_[tt]  # indices en muestra del inicio estimulo -> tomar 2 seg antes y 5 seg despues
    i_clases = i_clases_[tt]  # tipo de clase

    # i_muestras = i_muestras_ # indices en muestra del inicio estimulo -> tomar 2 seg antes y 5 seg despues
    # i_clases = i_clases_ # tipo de clase

    #eli = 1023
    #ind = i_clases_ != eli
    #i_clases = i_clases_[ind]
    #i_muestras = i_muestras_[ind]
    ni = np.zeros(len(clases))
    for i in range(len(clases)):
        ni[i] = np.sum(i_clases == clases[i])  # izquierda

    Xraw = np.zeros((int(np.sum(ni)), len(Ch), int(sfreq * (vt[1] + vt[0]))))
    y = np.zeros(int(np.sum(ni)))
    ii = 0
    for i in range(len(clases)):
        for j in range(len(i_clases)):
            if i_clases[j] == clases[i]:
                rc = raw[:, int(i_muestras[j] - vt[0] * sfreq):int(i_muestras[j] + vt[1] * sfreq)][0]
                rc = rc - np.mean(rc)
                Xraw[ii, :, :] = rc[Ch, :]
                y[ii] = int(i + 1)
                ii += 1

    return i_muestras, i_clases, raw, Xraw, y, ni, m

# %%


def leer_bci42a_test_full(path_filename, clases, Ch, vt):

    raw = read_raw_edf(path_filename, preload=False)
    sfreq = raw.info['sfreq']

    # raw.save('tempraw.fif',overwrite=True)#, tmin=3, tmax=5,overwrite = True)
    # rawo = mne.io.read_raw_fif('tempraw.fif', preload=True)  # load data
    # depurar canales
    # rawo.plot()

    # clases_b = [769,770,771,772] #codigo clases
    i_muestras_ = raw._raw_extras[0]['events'][1]           # Indices de las actividades.
    i_clases_ = raw._raw_extras[0]['events'][2]           # Marcadores de las actividades.

    # remov   = np.ndarray.tolist(i_clases_)                 # Quitar artefactos.
#    Trials_eli = 1023                                   # Elimina los trials con artefactos.
#    m       = np.array([i for i,x in enumerate(remov) if x==Trials_eli])   # Identifica en donde se encuentra los artefactos.
#    m_      = m+1
#    tt      = np.array(raw._raw_extras[0]['events'][0]*[1],dtype=bool)
#    tt[m]   = False
#    tt[m_]  = False
#    i_muestras = i_muestras_[tt] # indices en muestra del inicio estimulo -> tomar 2 seg antes y 5 seg despues
#    i_clases = i_clases_[tt] # tipo de clase
#
    i_muestras = i_muestras_  # indices en muestra del inicio estimulo -> tomar 2 seg antes y 5 seg despues
    i_clases = i_clases_  # tipo de clase

    ni = np.zeros(len(clases))
    for i in range(len(clases)):
        ni[i] = np.sum(i_clases == clases[i])  # izquierda

    Xraw = np.zeros((int(np.sum(ni)), len(Ch), int(sfreq * (vt[1] + vt[0]))))
    #y = np.zeros(int(np.sum(ni)))
    ii = 0
    for i in range(len(clases)):
        for j in range(len(i_clases)):
            if i_clases[j] == clases[i]:
                rc = raw[:, int(i_muestras[j] - vt[0] * sfreq):int(i_muestras[j] + vt[1] * sfreq)][0]
                rc = rc - np.mean(rc)
                Xraw[ii, :, :] = rc[Ch, :]
                #y[ii] = int(clases[i])
                ii += 1

    return i_muestras, i_clases, raw, Xraw

# %% Filters


def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = lfilter(b, a, data)
    return y

# %% Bank filter


def bank_filter_epochsEEG(Xraw, fs, f_frec):  # Xraw[nepochs,nchannels]
    nf, ff = f_frec.shape
    epochs, channels, T = Xraw.shape
    Xraw_f = np.zeros((epochs, channels, T, nf))
    for f in range(nf):
        lfc = f_frec[f, 0]
        hfc = f_frec[f, 1]
        b, a = butter_bandpass(lfc, hfc, fs)
        zi = lfilter_zi(b, a)
        for n in range(epochs):
            for c in range(channels):
                # print(c)
                zi = lfilter_zi(b, a)
                Xraw_f[n, c, :, f] = lfilter(b, a, Xraw[n, c, :], zi=zi * Xraw[n, c, 0])[0]
                #Xraw_f[n,c,:,f] = lfilter(b, a, Xraw[n,c,:])
    return Xraw_f

# %% CSP epochs


def CSP_epochsEEG(Xraw, y, ncomp):  # Xraw[nepochs,nchannels]

    csp = CSP(n_components=ncomp, reg='pca', log=True, norm_trace=False)
    epochs, channels, T, nf = Xraw.shape
    Xcsp = np.zeros((epochs, ncomp, nf))
    csp_l = []
    for f in range(nf):

        csp_l.append(csp.fit(Xraw[:, :, :, f], y))
        Xcsp[:, :, f] = csp_l[f].transform(Xraw[:, :, :, f])

    return csp_l, Xcsp


# %% CSP custom sklearn
from sklearn.base import BaseEstimator, TransformerMixin


class CSP_epochs_filter_extractor(TransformerMixin, BaseEstimator):
    def __init__(self, fs, f_frec=[4, 30], ncomp=4):
        self.fs = fs
        self.f_frec = f_frec
        self.ncomp = ncomp

    def _averagingEEG(self, X):

        epochs, channels, T = X.shape
        Xc = np.zeros((epochs, channels, T))
        for i in range(epochs):
            Xc[i, :, :] = X[i, :, :] - np.mean(X[i, :, :])
        return Xc

    def _bank_filter_epochsEEG(self, X):
        nf, ff = self.f_frec.shape
        epochs, channels, T = X.shape
        X_f = np.zeros((epochs, channels, T, nf))
        for f in range(nf):
            lfc = self.f_frec[f, 0]
            hfc = self.f_frec[f, 1]
            b, a = butter_bandpass(lfc, hfc, self.fs)
            zi = lfilter_zi(b, a)
            for n in range(epochs):
                for c in range(channels):
                    zi = lfilter_zi(b, a)
                    X_f[n, c, :, f] = lfilter(b, a, X[n, c, :], zi=zi * X[n, c, 0])[0]
        return X_f

    def _CSP_epochsEEG(self, Xraw, y, *_):
        ncomp = self.ncomp
        csp = CSP(n_components=ncomp, reg='empirical', log=True, norm_trace=False, transform_into='average_power')
        epochs, channels, T, nf = Xraw.shape
        Xcsp = np.zeros((epochs, self.ncomp, nf))
        csp_l = []
        for f in range(nf):
            csp_l.append(csp.fit(Xraw[:, :, :, f], y))
            Xcsp[:, :, f] = csp_l[f].transform(Xraw[:, :, :, f])
        return csp_l, Xcsp

    def fit(self, Xraw, y, *_):
        Xraw = self._averagingEEG(Xraw)
        Xraw_f = self._bank_filter_epochsEEG(Xraw)
        self.csp_l, self.Xcsp = self._CSP_epochsEEG(Xraw_f, y)
        return self

    def transform(self, Xraw, *_):
        Xraw = self._averagingEEG(Xraw)
        Xraw_f = self._bank_filter_epochsEEG(Xraw)
        epochs, channels, T, nf = Xraw_f.shape
        ncomp = self.ncomp
        result = np.zeros((epochs, ncomp, nf))
        for f in range(nf):
            result[:, :, f] = self.csp_l[f].transform(Xraw_f[:, :, :, f])
        result = result.reshape(np.size(result, 0), -1)
        return result

# %% Ejemplo filtro pasa bandas
#lfc = 4
#hfc = 8
#fs = raw.info['sfreq']
#b,a = butter_bandpass(lfc, hfc, fs,order=5)
#w, h = signal.freqz(b,a)
#angles = np.unwrap(np.angle(h))
# plt.figure(figsize=(15,15))
# plt.subplot(211)
#plt.plot((fs*w)/(2*np.pi), abs(h), 'b')
#plt.ylabel('Amplitud', color='b')
#plt.xlabel('Frecuencia [rad/sample]')
# plt.grid()
# plt.subplot(212)
#plt.plot((fs*w)/(2*np.pi), angles, 'g')
#plt.ylabel('Fase (radianes)')
# plt.grid()
# plt.show()
#
#zi = lfilter_zi(b, a)
#Xrawf = lfilter(b, a, Xraw,zi = zi*Xraw[0,0,0],axis=2)

# plt.plot(Xraw[0,0,:])
# plt.show()
# plt.plot(Xrawf[0],'r')

# %% leer csv openbci


def leer_openbci(filename, clases, Ch, vt):
    filenamej = filename.replace(".csv", ".json")
    with open(filenamej, 'r') as myfile:
        data = myfile.read()
    obj = js.loads(data)
    ss = obj['sample_rate']
    sfreq = float(ss[:-3])
    df = pd.read_csv(filename)
    arr = df.values[:, 0:-1]
    lab = df.values[:, -1]
    ni = np.zeros(len(clases))
    for i in range(len(clases)):
        ni[i] = np.sum(lab == clases[i])  # izquierda

    Xraw = np.zeros((int(np.sum(ni)), len(Ch), int(sfreq * (vt[1] + vt[0]))))
    y = np.zeros(int(np.sum(ni)))
    ii = 0
    for i in range(len(clases)):
        for n in range(np.size(arr, 0)):
              if lab[n] == clases[i]:
                    # rc = arr[int(n - vt[0] * sfreq):int(n + vt[1] * sfreq), 0:-1]
                    rc = arr[int(n - vt[0] * sfreq):int(n + vt[1] * sfreq), :]
                    rc = rc - np.mean(rc)
                    Xraw[ii, :, :] = rc[:, Ch].T
                    y[ii] = i + 1  # int(clases[i])
                    ii += 1

    return Xraw, y, sfreq


# %%
import time


def wait_BCI(prob, fac_max, tmax, tmin):
    r = 1 - fac_max
    m = (tmax - tmin) / (r - 0)  # tmax -> r ; tmin -> 0
    b = tmin
    ns = m * (prob - fac_max) + b
    time.sleep(ns)
    return ns
