# -*- coding: utf-8 -*-
"""
Created on Wed Jul 17 19:36:41 2019
@author: andre
"""
#%%
import numpy as np
from MIfunctions import leer_bci42a_train_full, leer_bci42a_test_full, bank_filter_epochsEEG, CSP_epochs_filter_extractor
import matplotlib.pyplot as plt
import scipy.io

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.svm import SVC
from sklearn.linear_model import SGDClassifier

from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split, GridSearchCV,cross_val_score
from sklearn.metrics import confusion_matrix, f1_score, accuracy_score
#%% read data
name = 'C:/Users/andre/Downloads/BCI_data/A08'
#name = 'A08'
##leer GDF train
filename_train = name+'T.gdf'
Ch = np.arange(0,22)#np.arange(7,15)
vt = [-1,4]#2*sfreq otf = 2 #5*sfreq
clases = [769,770]
i_muestras, i_clases, raw, Xraw, y, ni,m = leer_bci42a_train_full(filename_train,clases,Ch,vt)
print(Xraw.shape)
#%% CSP custom bank filter + csp 8--12 12--30
#f_frec = np.array([[4,8],[8,12],[12,16],[16,20],[20,24],[24,28]])
f_frec = np.array([[8,12],[12,30]])
fs = raw.info['sfreq']
ncomp = 8
import warnings
warnings.filterwarnings("ignore")
csp_epochs = CSP_epochs_filter_extractor(fs=fs,f_frec=f_frec, ncomp=ncomp)
csp_epochs.fit(Xraw,y)

#%% pca
scaler = StandardScaler()
Xcsp = csp_epochs.transform(Xraw)
Xdataz = scaler.fit_transform(Xcsp)
pca = PCA(n_components=min(Xcsp.shape))
#tsne = TSNE(n_components=2, perplexity=20.0)
#Z = tsne.fit_transform(Xdataz) 
Z = pca.fit_transform(Xdataz)  
plt.scatter(Z[:,0],Z[:,1],c=y)
plt.show()
Xdataz.shape
#%%
#entrenar modelo basado en csp y svc

steps = [('CSP_EEG',  CSP_epochs_filter_extractor(fs=fs,f_frec=f_frec)), 
         ('scaler', StandardScaler()), ('PCA', PCA(n_components=0.95)), ('SVM', SVC())]

pipeline = Pipeline(steps) # define the pipeline object.
X_train, X_test, y_train, y_test = train_test_split(Xraw,y,test_size=0.2, random_state=30, stratify=y)

#busqueda parametros CV
#parameters = {'SVM__C':[0.001,0.1,10,100,10e3], 'SVM__gamma':[0.1,0.01,1,10]}
parameters = {'CSP_EEG__ncomp':[4,8,10],'SVM__C':[0.1,10,100], 'SVM__gamma':[0.1,0.01,1,10]}
grid_search = GridSearchCV(pipeline, parameters, n_jobs=4,cv=5,scoring='accuracy',verbose=10)
grid_search.fit(Xraw, y)
#update joblib conda Anaconda 3 -> conda install joblib, conda update joblib

#%%
#mejor modelo
best_model = grid_search.best_estimator_
print(grid_search.best_params_)

#evaluar mejor modelo segun test desde split
y_test_e = best_model.predict(X_test)
cm = confusion_matrix(y_test, y_test_e)
cm = 100*cm/np.sum(cm,1)
print('\nConfusion matrix'+filename_train)
print(cm)
print("F1=%.4f" % f1_score(y_test, y_test_e)) 
print("Acc=%.4f" % accuracy_score(y_test, y_test_e)) 


#%% cv
best_model = grid_search.best_estimator_
scores = cross_val_score(best_model, Xraw, y, cv=10, n_jobs = -1,scoring='accuracy')#'f1')https://scikit-learn.org/stable/modules/model_evaluation.html
scores

#%%
#Evaluar sobre conjunto E
tfile = name+'E.mat'
mat = scipy.io.loadmat(tfile)
tlabels = mat['classlabel'] #left 1 (-1), right 2 (1)
clasesE = [768]#783,768cue unknown
filename_E = name+'E.gdf'
i_muestrasE, i_clasesE, rawE, XrawE = leer_bci42a_test_full(filename_E,clasesE,Ch,vt)
#clase 1 y 2 izq y der
ind = np.where((tlabels == 1) | (tlabels == 2))[0]
yE = tlabels[ind]
XrawE = XrawE[ind,:,:]
XrawE.shape

#%%

#estimar salida
yE_e = best_model.predict(XrawE)
cm_E = confusion_matrix(yE, yE_e)
cm_E = 100*cm_E/np.sum(cm_E,1)
print('\nConfusion matrix:'+tfile)
print(cm_E)
print("F1=%.4f" % f1_score(yE, yE_e)) 
print("Acc=%.4f" % accuracy_score(yE, yE_e)) 